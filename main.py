# main.py


def main():
    print("Welcome to the Git Branching and Merging Tutorial with a Conflict!")
    print("Made with love in Grenoble!")
    print("Enjoy learning!")


def greet_user(username):
    print(f"Hello, {username}! We hope you enjoy the tutorial!")


def feature3_function1():
    print("This is a feature3 function1.")


def feature2_function1():
    print("This is a feature2 function1.")


def feature2_function2():
    print("This is a feature2 function2.")


def feature1_function1():
    print("This is a feature1 function1.")


def feature1_function2():
    print("This is a feature1 function2.")


def feature1_function3():
    print("This is a feature1 function3.")


if __name__ == "__main__":
    main()
